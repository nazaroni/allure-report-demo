[![pipeline status](https://gitlab.com/duerrdental/vistor/e2e/vsm-e2e-tests/badges/master/pipeline.svg)](https://gitlab.com/duerrdental/vistor/e2e/vsm-e2e-tests/-/commits/master)
# PROD


we need to change line 24 to:
```shell
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
```
2

Now our system is ready for device simulation and testing

:white_check_mark: [Test report](https://nazaroni.gitlab.io/allure-report-demo/prod) (prod branch) :microscope:
