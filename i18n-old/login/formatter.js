import _template from 'lodash/template'
import branding from 'src/branding'

export default class Formatter {
  interpolate (message, values) {
    const compiled = _template(message)
    return [compiled({ branding, ...values })]
  }
}
