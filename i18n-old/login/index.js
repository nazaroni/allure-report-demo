import da from 'src/i18n-old/da/da'
import de from 'src/i18n-old/de/de'
import en from 'src/i18n-old/en/en'
import es from 'src/i18n-old/es/es'
import fr from 'src/i18n-old/fr/fr'
import it from 'src/i18n-old/it/it'
import ko from 'src/i18n-old/ko/ko'
import nl from 'src/i18n-old/nl/nl'
import pl from 'src/i18n-old/pl/pl'
import pt from 'src/i18n-old/pt/pt'
import ru from 'src/i18n-old/ru/ru'
import zh from 'src/i18n-old/zh/zh'
import xx from 'src/i18n-old/xx/xx'

export default {
  DEFAULT_LANGUAGE: 'en',
  SUPPORTED_LANGUAGES: [
    'da',
    'de',
    'en',
    'es',
    'fr',
    'it',
    'ko',
    'nl',
    'pl',
    'pt',
    'ru',
    'zh',
    'xx'
  ],
  messages: {
    da,
    de,
    en,
    es,
    fr,
    it,
    ko,
    nl,
    pl,
    pt,
    ru,
    zh,
    xx
  }
}
