export default async ({ $api }, locale) => {
  const [common, _api] = await Promise.all([
    import(/* webpackChunkName: `lang-[request]` */ `~/lang/${locale}.json`),
    loadFromApi($api, locale)
  ])
  const translations = {
    ...common,
    _api
  }
  return translations
}

async function loadFromApi($api, locale) {
  try {
    const res = await $api.i18n.get(locale)
    return { ...res.data }
  } catch (err) {
    return {}
  }
}

export const langSettings = {
  lazy: true,
  langDir: 'lang/',
  defaultLocale: 'en',
  strategy: 'no_prefix',
  parsePages: false,
  locales: [
    {
      code: 'da',
      name: 'Dansk',
      file: 'index.js'
    },
    {
      code: 'de',
      name: 'Deutsch',
      file: 'index.js'
    },
    {
      code: 'en',
      name: 'English',
      file: 'index.js'
    },
    {
      code: 'es',
      name: 'Español',
      file: 'index.js'
    },
    {
      code: 'fr',
      name: 'Français',
      file: 'index.js'
    },
    {
      code: 'it',
      name: 'Italiano',
      file: 'index.js'
    },
    {
      code: 'ko',
      name: '한국어',
      file: 'index.js'
    },
    {
      code: 'nl',
      name: 'Nederlands',
      file: 'index.js'
    },
    {
      code: 'pl',
      name: 'Polski',
      file: 'index.js'
    },
    {
      code: 'pt',
      name: 'Português',
      file: 'index.js'
    },
    {
      code: 'ru',
      name: 'Русский',
      file: 'index.js'
    },
    {
      code: 'zh',
      name: '中文',
      file: 'index.js'
    }
  ]
}
