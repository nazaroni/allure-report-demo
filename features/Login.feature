@sequential
Feature: Login test

  Background: Register fake user
    Given user located on the login page

  Scenario: Login test
    When enter "registered E-Mail" address in the corresponding field
      And enter "registered Password" in the corresponding password field
      And press "LOG-IN" button
    Then successful system feedback, VSM is opened
      And logout
