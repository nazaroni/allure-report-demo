# -*- coding: utf-8 -*-
import logging

from behave import Given, When, Then
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from assertpy import assert_that

logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s')


@Given('user located on the login page')
@When('user located on the login page')
def get_login_page(context):
    context.driver.get(context.keywords['web_page'])
    context.wait.until(ec.title_contains('Dürr Dental Login'))
    xpath = '//*[@type="email"]'
    context.wait.until(ec.visibility_of_element_located((By.XPATH, xpath)))

    # context.wait.until(ec.visibility_of(context.pages.login.email_field))


@Then('user located on the VSM page')
@When('visit VSM page')
def get_vsm_page(context):
    context.driver.get(context.keywords['web_page'])
    context.wait.until(ec.visibility_of(context.pages.vsm.vsm_username_icon))
    assert_that(context.driver.title).is_equal_to('VSM - VistaSoft Monitor')


@When('enter valid email address in the corresponding field')
def login_enter_registered_email(context):
    context.pages.login.email_field.send_keys(context.keywords['registered E-Mail'])


@When('enter "{email}" address in the corresponding field')
def service_partner_enter_email(context, email):
    print(context.keywords[f'{email}'])
    context.pages.login.email_field.send_keys(context.keywords[f'{email}'])


@When('enter "{password}" in the corresponding password field')
def login_enter_password(context, password):
    context.pages.login.password_field.send_keys(context.keywords[f'{password}'])


@When('press "LOG-IN" button')
@Then('press "LOG-IN" button')
def login_press_login_button(context):
    context.pages.login.login_button.click()


@When('press "Login" button in maintenance page')
def maintenance_press_login(context):
    context.pages.login.login_button_maintenance.click()


@When('successful system feedback, VSM is opened')
@Then('successful system feedback, VSM is opened')
def vsm_is_opened(context):
    context.wait.until(ec.visibility_of(context.pages.vsm.vsm_username_icon))
    context.wait.until(ec.title_contains('VSM - VistaSoft Monitor'))


@Then('logout')
def vsm_logout(context):
    context.pages.vsm.vsm_username_icon.click()
    context.pages.vsm.user_menu_logout.click()


@When('visit some other webpage')
def visit_other_webpage(context):
    context.driver.get('https://duckduckgo.com')
    context.wait.until(ec.title_contains('DuckDuckGo'))
    assert_that(context.driver.title).contains('DuckDuckGo')


@When('successful system feedback, redirect to Login Page')
def refresh_back_to_login(context):
    context.wait.until(ec.url_contains(context.keywords['login_page']), 20)


@When('user view invite email send by duerrdental')
def open_invite_email(context):
    try:
        context.pages.outlook.invite_email.click()
        context.pages.outlook.invite_link.click()
    except TimeoutException:
        context.pages.outlook.other_inbox.click()
        context.pages.outlook.invite_email.click()
        context.pages.outlook.invite_link.click()
    context.pages.outlook.delete_btn.click()
    context.wait.until(ec.visibility_of(context.pages.outlook.verify_email_deleted))
    context.driver.close()
    context.driver.switch_to_window(context.driver.window_handles[0])
    context.driver.close()
    context.driver.switch_to_window(context.driver.window_handles[0])


@When('user view confirmation email send by duerrdental')
def open_confirmation_email(context):
    try:
        context.pages.outlook.confirmation_email.click()
        context.pages.outlook.confirmation_link.click()
    except TimeoutException:
        context.pages.outlook.other_inbox.click()
        context.pages.outlook.confirmation_email.click()
        context.pages.outlook.confirmation_link.click()
    context.pages.outlook.delete_btn.click()
    context.wait.until(ec.visibility_of(context.pages.outlook.verify_email_deleted))
    context.driver.close()
    context.driver.switch_to_window(context.driver.window_handles[1])
    context.driver.close()
    context.driver.switch_to_window(context.driver.window_handles[0])
