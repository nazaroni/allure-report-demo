# -*- coding: utf-8 -*-
import types
import os

import allure
import allure_commons

from multiprocessing import cpu_count
from behave import fixture, use_fixture
from concurrent.futures import ThreadPoolExecutor
from selenium import webdriver
from behave.log_capture import capture
from dotenv import load_dotenv
from selenium.webdriver.support.wait import WebDriverWait
from datetime import datetime
from features.lib.logger import log
from features.lib.locators import Locator
from features.lib.i18n.i18n import get_translation
from features.lib.pages.login_page import LoginPage
from features.lib.pages.vsm_page import VSMPage
from features.lib.pages.thanks_page import ThanksPage
from features.lib.utils import get_user_tokens, delete_user

# Get environment variables
load_dotenv()

BROWSER_TYPE = os.getenv('BROWSER_TYPE') or 'Chrome'
BROWSER_LANG = os.getenv('BROWSER_LANG') or 'en'

if os.getenv('ENVIRONMENT') is None:
    os.environ['ENVIRONMENT'] = 'stage'

if os.environ['ENVIRONMENT'].lower() == 'test':
    WEBSITE_PAGE = 'http://localhost:8080'
    LOGIN_PAGE = 'https://dev.login.vistasoftmonitor.com'
    EXISTING_USER_EMAIL = os.getenv('TEST_USER_EMAIL')
    EXISTING_USER_PASS = os.getenv('TEST_USER_PASS')
    EXISTING_REAL_PASS = os.getenv('TEST_REAL_PASS')
    AUTH_ENDPOINT = os.getenv('TEST_AUTH_ENDPOINT')
    VSM_ENDPOINT = os.getenv('TEST_VSM_ENDPOINT')
    API_KEY = os.getenv('TEST_API_KEY')
elif os.environ['ENVIRONMENT'].lower() == 'dev':
    WEBSITE_PAGE = 'https://dev.vistasoftmonitor.com'
    LOGIN_PAGE = 'https://dev.login.vistasoftmonitor.com'
    EXISTING_USER_EMAIL = os.getenv('DEV_USER_EMAIL')
    EXISTING_USER_PASS = os.getenv('DEV_USER_PASS')
    EXISTING_REAL_PASS = os.getenv('DEV_REAL_PASS')
    AUTH_ENDPOINT = os.getenv('DEV_AUTH_ENDPOINT')
    VSM_ENDPOINT = os.getenv('DEV_VSM_ENDPOINT')
    API_KEY = os.getenv('DEV_API_KEY')
elif os.environ['ENVIRONMENT'].lower() == 'stage':
    WEBSITE_PAGE = 'https://neo.vistasoftmonitor.com'
    LOGIN_PAGE = 'https://staging.login.vistasoftmonitor.com'
    EXISTING_USER_EMAIL = 'dd_test_5@outlook.com'
    EXISTING_USER_PASS = '7[rLtRD}^'
    AUTH_ENDPOINT = 'https://api.duerrdental.com/login_stage/'
    VSM_ENDPOINT = 'https://api.duerrdental.com/stage/'
    API_KEY = '5F5DEqhl01atCLIbRi3YP5qgGr6Qrbhr9MW4dkvl'
elif os.environ['ENVIRONMENT'].lower() == 'prod':
    WEBSITE_PAGE = 'https://cloud.duerrdental.com'
    LOGIN_PAGE = 'https://login.duerrdental.com'
    EXISTING_USER_EMAIL = 'dd_test_2@outlook.com'
    EXISTING_USER_PASS = 'tDP4mYJ'
    AUTH_ENDPOINT = 'https://api.duerrdental.com/login_prod/'
    VSM_ENDPOINT = 'https://api.duerrdental.com/prod/'
    API_KEY = 'KHo3vRcIig4Y95VFEQs6a3uuBJ997eGXM4EBv1fg'
else:
    raise Exception(
        'unable to run behaviour tests, environment unknown: {}', os.environ.get('ENVIRONMENT'))


MAP_KEYWORDS = {
    'web_page': WEBSITE_PAGE,
    'login_page': LOGIN_PAGE,
    'auth_endpoint': AUTH_ENDPOINT,
    'registered E-Mail': EXISTING_USER_EMAIL,
    'registered Password': EXISTING_USER_PASS,
}

vsm_translation_json = get_translation(BROWSER_LANG, './i18n-old/vsm')
login_translation_json = get_translation(BROWSER_LANG, f'./i18n-old/login/{BROWSER_LANG}/')
cpu_workers = cpu_count() if cpu_count() < 2 else cpu_count() - 1


@fixture
def selenium_browser(context):
    if BROWSER_TYPE == 'Firefox':
        options = webdriver.FirefoxOptions()
        options.headless = True
        profile = webdriver.FirefoxProfile()
        profile.set_preference('intl.accept_languages', BROWSER_LANG)
        context.driver = webdriver.Firefox(firefox_profile=profile, options=options)
        context.driver.set_window_size(1920, 1080)
        context.driver.maximize_window()
        context.driver.implicitly_wait(0.5)
    else:
        options = webdriver.ChromeOptions()
        options.add_argument(f'--lang={BROWSER_LANG}')
        options.add_argument('--window-size=1920,1080')
        prefs = {'download.default_directory': os.path.join(f'{os.getcwd()}', 'tmp')}
        options.add_experimental_option('prefs', prefs)
        options.add_experimental_option('detach', True)
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('--disable-software-rasterizer')
        context.driver = webdriver.Chrome(chrome_options=options)
        context.driver.maximize_window()
        context.driver.implicitly_wait(0.5)

    context.wait = WebDriverWait(context.driver, 20)
    context.locator = Locator

    yield context.driver


@fixture
def map_keywords(context):
    context.keywords = MAP_KEYWORDS
    if vsm_translation_json is not None:
        context.vsm_i18n = vsm_translation_json
    if login_translation_json is not None:
        context.login_i18n = login_translation_json


@fixture
def add_pages(context):
    context.pages = types.SimpleNamespace()
    context.pages.login = LoginPage(context)
    context.pages.vsm = VSMPage(context)
    context.pages.thanks = ThanksPage(context)


@fixture
def create_threadpool(context):
    context.threadpool = {'executor': ThreadPoolExecutor(max_workers=cpu_workers), 'futures': []}
    yield context.threadpool['executor']
    context.threadpool['executor'].shutdown(wait=True)


def before_tag(context, tag):
    if tag == 'fixture.threadpool.create':
        use_fixture(create_threadpool, context)


@capture
def before_scenario(context, scenario):
    log.info(f'Scenario: "{scenario.name}"')
    use_fixture(selenium_browser, context)
    use_fixture(map_keywords, context)
    use_fixture(add_pages, context)


@capture
def after_scenario(context, scenario):
    if scenario.status == 'failed':
        # save screenshot as soon as possible to allure
        allure.attach(context.driver.get_screenshot_as_png(),
                      name='screenshot',
                      attachment_type=allure.attachment_type.PNG)

        # save screenshot for local debugging
        context.driver.save_screenshot(f'tmp/fail_{scenario.name}_{datetime.now().strftime("%Y_%m_%d__%h_%m_%s")}.png')

    context.driver.quit()


def before_step(context, step):
    name = f'{step.keyword} {step.name}'
    allure_commons.plugin_manager.hook.start_step(uuid=None, title=name, params={})


def after_step(context, step):
    allure_commons.plugin_manager.hook.stop_step(uuid=None, title=None,
                                                 exc_type=type(step.exception),
                                                 exc_val=step.exception,
                                                 exc_tb=step.exc_traceback)
