# -*- coding: utf-8 -*-
import os

from random import randint
from dotenv import load_dotenv
from datetime import date

load_dotenv()


class FakeUser:
    def __init__(self):
        self.name = 'Fake User'
        self.email = f'{os.getenv("NEW_USER_EMAIL_HEAD")}_{date.today().strftime("%Y-%m-%d")}_{str(randint(10000, 99999))}{os.getenv("NEW_USER_EMAIL_END")}'
        self.password = f'{os.getenv("NEW_USER_PASS_HEAD")}{str(randint(10000, 99999))}{os.getenv("NEW_USER_PASS_END")}'
        self.new_password = os.getenv('TEST_PASSWORD_CHANGE')
        self.serial_no = f'T{str(randint(10000, 99999))}'
