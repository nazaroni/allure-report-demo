# -*- coding: utf-8 -*-
# internationalisation. Find and return correct
import errno
import json
from os import strerror
from os.path import join, exists


def get_translation(lang_iso, path2lang):
    translation_file = join(path2lang, f'{lang_iso}.json')
    if exists(translation_file):
        with open(translation_file, encoding='utf-8') as json_file:
            translation_json = json.load(json_file)
            return translation_json
    else:
        raise FileNotFoundError(errno.ENOENT, strerror(errno.ENOENT), translation_file)
