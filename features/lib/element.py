# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class Element:
    def __init__(self, element: WebElement, driver: webdriver):
        self.web_element = element
        self.driver = driver
        self.wait = WebDriverWait(element, 15)

    def __getattr__(self, name):
        return getattr(self.web_element, name)

    def get(self, by: (By, str), cond=ec.presence_of_element_located):
        return Element(self.wait.until(cond(by)), self.driver)

    def getx(self, xpath: str, cond=ec.presence_of_element_located):
        by = (By.XPATH, xpath)
        return self.get(by, cond)

    def check_element_presence(self, xpath: str):
        try:
            self.driver.find_element_by_xpath(xpath)
            return True
        except NoSuchElementException:
            return False

    def set_text(self, my_string):
        print(self.get_attribute('value'))
        self.clear()
        self.send_keys(my_string)
        self.getx(f"//input[@value='{my_string}']")

    def set_text_empty_field(self, my_string):
        self.send_keys(my_string)
