# -*- coding: utf-8 -*-
import os
import base64
import logging
import json
import requests
import email
import re
import imaplib
from dotenv import load_dotenv
from time import sleep

load_dotenv()
logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.INFO)


def get_user_tokens(user_email, user_password, auth_endpoint):
    header_value = f'{user_email}:{user_password}'
    base64_header = base64.b64encode(header_value.encode())
    headers = {'Authorization': f'Basic {base64_header.decode()}', 'x-api-key': os.getenv('OAUTH_KEY')}
    body = f'response_type=refresh_token&grant_type=client_credentials&client_id={user_email}'
    r = requests.post(auth_endpoint + os.getenv('AUTHORIZE_ENDPOINT'), data=body, headers=headers)
    if r.status_code == 200:
        access_token = json.loads(r.text)['access_token']
        refresh_token = json.loads(r.text)['refresh_token']
        return access_token, refresh_token
    else:
        access_token = None
        refresh_token = None
        return access_token, refresh_token


def delete_user(user_access_token, api_key, vsm_endpoint):
    headers = {'Authorization': f'Bearer {user_access_token}', 'x-api-key': api_key}
    requests.delete(vsm_endpoint + 'user', headers=headers)


def email_get_confirmation_code(username, password):
    sleep(5)
    imap = imaplib.IMAP4_SSL('outlook.office365.com')
    imap.login(username, password)

    status, messages = imap.select('INBOX')
    messages = int(messages[0])  # this is how many mails you have
    my_code = ''

    if messages:
        content_type, body, = '', ''

        res, msg = imap.fetch(str(messages), '(RFC822)')
        for response in msg:
            if isinstance(response, tuple):
                # parse a bytes email into a message object
                msg = email.message_from_bytes(response[1])

                # if the email message is multipart
                if msg.is_multipart():
                    # iterate over email parts
                    for part in msg.walk():
                        # extract content type of email
                        content_type = part.get_content_type()
                        try:
                            # get the email body
                            body = part.get_payload(decode=True).decode()
                        except Exception:
                            pass

                if content_type == 'text/html':
                    my_code = re.findall(r'\d{6}', body)[0]

    # close the connection and logout
    imap.close()
    imap.logout()
    return my_code
