# -*- coding: utf-8 -*-
from features.lib.element import Element
from features.lib.pages.base_page_object import BasePageAction
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec


class VSMPage(BasePageAction):
    def __init__(self, context):
        BasePageAction.__init__(self, context)

    def find_root(self) -> Element:
        self.context.wait.until(ec.visibility_of_element_located((
            By.XPATH, '//button[@class[contains(., "_tid_nav-user-button")]]')))
        el = self.context.driver.find_element_by_tag_name('body')
        return Element(el, self.context.driver)

    @property
    def vsm_map(self):
        return self.root.getx('.//a[@href="/map"]')

    @property
    def vsm_my_devices(self):
        return self.root.getx('.//a[@href="/things"]')

    @property
    def vsm_my_devices_title(self):
        return self.root.getx('.//a[@href="/things"]')

    @property
    def vsm_news(self):
        return self.root.getx('.//a[@href="/news"]')

    @property
    def vsm_favourites(self):
        return self.root.getx('.//a[@href="/favorites"]')

    @property
    def vsm_settings(self):
        self.root.getx(self.context.locator.animation, ec.invisibility_of_element)
        return self.root.getx('.//a[@href="/settings"]')

    @property
    def vsm_part_catalog(self):
        return self.root.getx('.//a[@href="/parts-catalog"]')

    @property
    def vsm_username_icon(self):
        return self.root.getx('.//button[@class[contains(., "_tid_nav-user-button")]]')

    @property
    def user_menu_logout(self):
        return self.root.getx(f'.//*[@role="menuitem"]/child::span[contains(text(), '
                              f'"{self.vsm_i18n["buttons"]["logout"]}")]')

    @property
    def page_loader(self):
        return self.root.getx('//*[@id="svg-loader"]')

    @property
    def search_option(self):
        return self.root.getx('.//a[@href="/search"]')

    @property
    def extend_warranty_option(self):
        self.root.getx(self.context.locator.animation, ec.invisibility_of_element)
        return self.root.getx('.//a[@href="/warranty/things"]')

    @property
    def user_settings_button(self):
        xpath = f'.//*[contains(text(),"{self.context.vsm_i18n["menu"]["nav_user_settings"]}")]' \
                f'/ancestor::div[@role="menuitem"]'
        return self.root.getx(xpath)

    @property
    def language_en_settings_button(self):
        self.root.getx(self.context.locator.animation, ec.invisibility_of_element)
        xpath = f'.//*[contains(text(),"{self.context.vsm_i18n["menu"]["nav_lang"]}")]/ancestor::div[@role="menuitem"]'
        return self.root.getx(xpath)

    @property
    def it_language_button(self):
        self.root.getx(self.context.locator.animation, ec.invisibility_of_element)
        return self.root.getx('.//*[contains(text(),"Italiano")]/ancestor::div[@role="menuitem"]')

    @property
    def language_it_settings_button(self):
        return self.root.getx('.//*[contains(text(),"Lingua")]/ancestor::div[@role="menuitem"]')

    @property
    def en_language_button(self):
        return self.root.getx('.//*[contains(text(),"English")]/ancestor::div[@role="menuitem"]')

    @property
    def successful_language_change(self):
        return self.root.getx('.//a[@href="/settings"]//descendant::span')

    @property
    def help_button(self):
        xpath = f'.//*[contains(text(),"{self.context.vsm_i18n["menu"]["help"]}")]/ancestor::div[@role="menuitem"]'
        return self.root.getx(xpath)

    @property
    def data_privacy_button(self):
        xpath = f'.//*[contains(text(),"{self.context.vsm_i18n["menu"]["nav_data_privacy"]}")]' \
                f'/ancestor::div[@role="menuitem"]'
        return self.root.getx(xpath)

    @property
    def user_logout_button(self):
        xpath = f'.//*[contains(text(),"{self.context.vsm_i18n["buttons"]["logout"]}")]/ancestor::div[@role="menuitem"]'
        return self.root.getx(xpath)

    @property
    def alert_message(self):
        return self.root.getx('//*[@class="flex items-start"]')

    @property
    def close_alert_message(self):
        return self.root.getx('.//span[@class="sr-only" and text()="Close"]/ancestor::button')

    @property
    def remove_account_checkbox(self):
        return self.root.getx('.//input[@type="checkbox"]')

    @property
    def confirmation_account_removal_button(self):
        return self.root.getx('.//button[contains(@class, "_tid_delete-confirm-btn")]')

    @property
    def cancel_account_removal_button(self):
        xpath = f'.//*[contains(text(),"{self.context.vsm_i18n["buttons"]["cancel"]}")]'
        return self.root.getx(xpath)

    @property
    def confirm_data_saved(self):
        return self.root.getx(f'//*[contains(text(), "{self.context.vsm_i18n["remote_operations"]["save_success"]}")]')

    @property
    def confirm_data_sent_to_device(self):
        # TODO: replace text with translation_json after update
        return self.root.getx('//*[contains(text(), "Data successfully sent to device")]  ')

    @property
    def confirm_changes_successful(self):
        # TODO: replace text with translation_json after update
        return self.root.getx('//*[contains(text(), "Changes successfully confirmed")]  ')
