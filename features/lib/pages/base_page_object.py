# -*- coding: utf-8 -*-
from features.lib.element import Element

'''
This class is the parent of all pages
in contains all the generic methods and utilities for all the pages
'''


class BasePageAction:

    def __init__(self, context, root=None):
        self.context = context
        self.vsm_i18n = context.vsm_i18n
        self.login_i18n = context.login_i18n

    @property
    def root(self):
        return self.find_root()

    def find_root(self) -> Element:
        raise NotImplementedError()
