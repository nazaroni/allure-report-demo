# -*- coding: utf-8 -*-
from features.lib.element import Element
from features.lib.pages.base_page_object import BasePageAction
from assertpy import assert_that
from selenium.webdriver.support import expected_conditions as ec


class ThanksPage(BasePageAction):
    def __init__(self, context):
        BasePageAction.__init__(self, context)

    def find_root(self) -> Element:
        self.context.wait.until(ec.url_contains(f"{self.context.keywords['login_page']}/register/thanks"))
        assert_that(self.context.driver.current_url).contains(f"{self.context.keywords['login_page']}/register/thanks")
        el = self.context.driver.find_element_by_tag_name('body')
        return Element(el, self.context.driver)

    @property
    def successful_register_message(self):
        return self.root.getx(f'//*[contains(text(), "{self.login_i18n["thanks"]}")]')

    @property
    def return_to_login_btn(self):
        self.root.getx(self.context.locator.animation, ec.invisibility_of_element)
        return self.root.getx('//*[@data-cy="login_button"]')
