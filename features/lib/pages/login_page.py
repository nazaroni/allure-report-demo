# -*- coding: utf-8 -*-
from features.lib.element import Element
from features.lib.pages.base_page_object import BasePageAction
from assertpy import assert_that


class LoginPage(BasePageAction):
    def __init__(self, context):
        BasePageAction.__init__(self, context)

    def find_root(self) -> Element:
        assert_that(self.context.driver.current_url).contains(self.context.keywords['login_page'])
        el = self.context.driver.find_element_by_tag_name('body')
        return Element(el, self.context.driver)

    @property
    def email_field(self):
        return self.root.getx('.//*[@type="email"]')

    @property
    def password_field(self):
        return self.root.getx('.//*[@type="password"]')

    @property
    def login_button(self):
        return self.root.getx('.//button[@type="submit"]')

    @property
    def login_button_maintenance(self):
        return self.root.getx(f'.//span[contains(text(), "{self.vsm_i18n["buttons"]["login"]}")]')

    @property
    def signup_button(self):
        return self.root.getx('.//*[@data-cy="register_link"]')

    @property
    def press_forget_password_button(self):
        return self.root.getx('//*[@data-cy="forgot_password_link"]')

    @property
    def success_reset_message(self):
        search_str = f'//*[contains(text(), "{self.context.login_i18n["password_change_message"]}")]'
        return self.root.getx(search_str)

    @property
    def username_icon(self):
        return self.root.getx('//*[@class="flex-shrink-0"]/ancestor::button')

    @property
    def login_text(self):
        return self.root.getx(f'//h1[contains(text(), "{self.context.login_i18n["login"]["headline"]}")]')
